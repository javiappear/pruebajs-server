**Repositorio donde reside la parte del servidor de la pruebaJS**

## Instalación de la base de datos (MongoDB)
Siguiendo los siguientes pasos se instalará la base de datos MongoDB en el sistema (OSX).

1. Actualizar el repositorio de paquetes brew 
```sh
brew update
```
2. Instalar el paquete MongoDB 
```sh
brew install mongodb
```
3. Crear el directorio donde se escribirán los ficheros de la base de datos 
```sh
mkdir -p /data/db
```
4. Ver el usuario del sistema whoami
```sh
whoami
```
5. Dar permisos al directorio 
```sh
sudo chown {USUARIO} /data/db
```
6. Inicializar el daemon de mongod
```sh
mongod
```
7. Inicializar el proceso mongo (desde otra terminal)
```sh
mongo
```
8. Seleccionar la base de datos a gestionar use db
```sh
use db
```
9. Crear un usuario
```sh
db.createUser({user: "usuario", pwd: "(m5]D:'4~F>E}v2j", roles: [{ role: "userAdminAnyDatabase", db: "admin"}]});
```

## Ejecutar aplicación Node + Express
1. Estando dentro de la carpeta raíz del proyecto instalamos las dependencias
```sh
npm install
```
2. Una vez se han instalado las dependencias, ejecutar el proyecto
```sh
npm run dev
```

## Ejecutar tests de la aplicación
Se han desarrollado tests para probar algunas funciones de los controladores, para probarlos ejecutar:
```sh
npm run testv1
```

## Documentación
1. Instalar de forma global apidocs
```sh
npm install apidoc -g
```
2. Generar la documentación 
```sh
npm run docv1
```
3. Acceder a la documentación acceder a /doc