// Include express to init the app
const express = require('express');

// Include mongoose, as it's the driver to use
const mongoose = require('mongoose');

// Include body parser of HTTP Requests
const bodyParser = require('body-parser');

// Globals to use
global.include = route => require(__dirname + '/' + route);
global.includev1 = route => require(__dirname + '/restful/v1/' + route);
global.port = 3000;
global.production = require('./environment');

// Init the app
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Establish the current version and init the architecture
const v1 = require('./restful/v1')
app.use('/', v1);

// Indicate mongoose the connection will be established with Promises
mongoose.Promise = global.Promise;

// Database connection
mongoose.connect('mongodb://localhost:27017/db', {useNewUrlParser: true}).then(() => {
    // Connection was succesful
    console.log("Conexión a la base de datos realizada correctamente");

    // Create the server in the specified port
    app.listen(port, () => {
        console.log(`Servidor escuchando en el puerto: ${port}`);
    });
}).catch(err => console.log(err));

// Load documentation for the API
app.use('/doc', express.static('public/docs'));

// Manage global errors
app.use((err, req, res, next) => {
    if (Object.prototype.isPrototypeOf.call(Error.prototype, err)) {
        return res.status(err.status || 500).json({error: err.message});
    }
});

process.on('unhandledRejection', error => {
    console.error('Unhandled rejection', error.message);
});

module.exports = app;