module.exports = function(req, res, next) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Content-Length, Accept, Origin, Referer, User-Agent');
    if ('OPTIONS' === req.method) return res.status(200).send();
    next();
}