// Import cron package
const cron = require('cron');

// Import update feeds service
const feedUpdateService = includev1('services/feed-update');

// seconds, minutes, hoours, day of the month, month, day of the week
// Process every morning at 5 o'clock
var jobUpdateFeeds = new cron.CronJob({
    cronTime: '0 0 5 * * *',
    start: true,
    onTick: function () {
        feedUpdateService.updateAllSources();
    },
    timeZone: 'Europe/Madrid'
});

// Start the job
jobUpdateFeeds.start();