// Import mongoose
const mongoose = require("mongoose");

// Import Feed model
const Feed = require('../../models/feed');

// Import testing packages
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../../../app');
const should = chai.should();

chai.use(chaiHttp);

const feedToInsertData = {
    "title": "Esto es un feed insertado haciendo tests",
    "body": "Este es el cuerpo de un feed de prueba",
    "image": "https://www.google.es",
    "source": "el_mundo",
    "publisher": "Javier de la Torre"
};

const feedToInsert = Feed(feedToInsertData);

/*
  * Test the /GET route
  */
describe('/GET feeds', () => {
    it('it should GET all the feeds', (done) => {
        chai.request(app)
            .get('/feeds')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.data.should.be.a('array');
                done();
            });
    });
});

/*
 * Test the /GET/:id route
 */
describe('/GET/:id book', () => {
    it('it should GET a feed by the given id', (done) => {
        feedToInsert.save((err, feed) => {
            chai.request(app)
                .get('/feeds/' + feed.id)
                .send(feed)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.data.should.be.a('object');
                    res.body.data.should.have.property('title');
                    res.body.data.should.have.property('body');
                    res.body.data.should.have.property('publisher');
                    res.body.data.should.have.property('_id').eql(feed.id);
                    ['el_pais', 'el_mundo', ''].should.contain(res.body.data.source);
                    done();
                });
        });
    });
});

/*
* Test the /POST route
*/
describe('/POST feeds', () => {
    it('it should POST a feed', (done) => {
        chai.request(app)
            .post('/feeds')
            .send(feedToInsertData)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('processTime');
                res.body.data.should.be.a('object');
                res.body.data.should.have.property('_id');
                done();
            });
    });
});