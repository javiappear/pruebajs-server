// Import express
const express = require('express');

// Import controllers
const controllers = require('./controllers');

// Retrieve the app
const app = express();

// Save times
app.use('*', (req, res, next) => {
    req.processInit = new Date().getTime();

    next();
});

// Load middlewares
app.use(require('./middlewares/access-control'));

// Assign the controllers of this version to the app
app.use(controllers);

// Include cron workers available for this version
require('./cron');

// Load error handlers of this version
app.use(includev1('errors/application/handler'));

module.exports = app;