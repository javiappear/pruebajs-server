// Include url validator
require('mongoose-type-url');

// Import mongoose
const mongoose = require('mongoose');

// Import Schema
const Schema = mongoose.Schema;

// Set name of the collection
const schemaName = 'feed';

const feedSchema = new Schema({
    title: {
        type: String,
        trim: true,
        required: true
    },
    body: {
        type: String,
        trim: true,
        required: true
    },
    image: {
        type: String,//mongoose.SchemaTypes.Url,
        trim: true,
        default: '',
    },
    source: {
        type: String,
        enum: ['el_pais', 'el_mundo', ''], // If no value => manual method
        default: ''
    },
    publisher: {
        type: String,
        trim: true,
        required: true
    }
}, {timestamps: true});

// I could have added some indexes in the title per example to speed up searches (in the case there were any)

feedSchema.virtual('periodico').get(function () {
    let periodico = '';

    if (!this.source) periodico = 'Añadido manualmente';
    else if (this.source === 'el_pais') periodico = 'El País';
    else periodico = 'El Mundo';

    return periodico;
});

feedSchema.set('toObject', {virtuals: true});

module.exports = mongoose.model(schemaName, feedSchema);