/**
 * @api {get} /feeds Get all the feeds
 * @apiVersion 1.0.0
 * @apiName GetFeeds
 * @apiGroup Feed
 *
 *
 * @apiSuccess {Object[]} data       List of feeds.
 * @apiSuccess {Number}   processTime    Time of the server to process request
 * @apiSuccess {Number} data.id Feed id
 * @apiSuccess {String} data.title Feed title
 * @apiSuccess {String} data.body Feed body
 * @apiSuccess {String} data.image Feed image
 * @apiSuccess {String} data.source Feed source
 * @apiSuccess {String} data.publisher Feed publisher
 * @apiSuccess {Date} data.createdAt Feed created at
 * @apiSuccess {Date} data.updatedAt Feed updated at
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
 *     "image": "https://www.google.es",
 *      "source": "el_mundo",
 *      "_id": "5b9e44e95fbab733c3835b7c",
 *      "title": "Esto es el primer feed insertado",
 *      "body": "Este es el cuerpo",
 *      "publisher": "Javier de la Torre",
 *      "createdAt": "2018-09-16T11:56:25.411Z",
 *      "updatedAt": "2018-09-16T11:56:25.411Z",
 * }
 *
 */