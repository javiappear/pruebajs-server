// Importar express
const express = require('express');

// Recuperar la aplicación
const app = express();

//Recuperar errores
const ApplicationError = require('./error');

//Función send
const send = require('./../../helpers/send');

// Manejador de errores
module.exports = (err, req, res, next) => {
    if (Object.prototype.isPrototypeOf.call(ApplicationError.prototype, err)) {
        send(req, res, {error: err.message + 'mod' || 'Unknown error'}, err.status || 500);//res.status(err.status || 500).json({error: err.message || 'Unknown error'});
    }
}