// Import express
const express = require('express');

// Retrieve the app
const app = express();

// Import feed service
const FeedService = includev1('services/feed');

// Import feed update service
const FeedUpdateService = includev1('services/feed-update');

// Import handler for each route, it will help us to manage errors
const controllerHandler = includev1('helpers/controllerHandler');

/*
 * GET /feeds route to retrieve all the feeds.
 */
app.get('', controllerHandler((req, res) => {
    const query = req.query || {};

    return FeedService.getFeeds(query);
}));

/*
 * GET /feeds/today route to delete a feed
 */
app.get('/today', controllerHandler(async (req, res) => {
    // Check if there are 0 elements to insert data
    if ((await FeedService.countFeeds()) === 0) {
        // Insert data
        await FeedUpdateService.updateAllSources();
    }

    return FeedService.todayFeeds();
}));

/*
 * GET /feeds/{id} route to retrieve a feed by ID.
 */
app.get('/:id', controllerHandler((req, res) => {
    const id = (req.params || {}).id;

    return FeedService.getFeedById(id);
}));

/*
 * POST /feeds route to insert a feed
 */
app.post('', controllerHandler((req, res) => {
    const data = req.body || {};

    return FeedService.insertFeed(data);
}));

/*
 * UPDATE /feeds/{id} route to update a feed
 */
app.put('/:id', controllerHandler((req, res) => {
    const id = (req.params || {}).id;
    const data = req.body || {};

    return FeedService.updateFeed(id, data);
}));

/*
 * DELETE /feeds/{id} route to delete a feed
 */
app.delete('/:id', controllerHandler((req, res) => {
    const id = (req.params || {}).id;

    return FeedService.deleteFeed(id);
}));

module.exports = app;