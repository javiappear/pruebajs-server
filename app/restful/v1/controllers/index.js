// Importar express
const express = require('express');

// Recuperar la aplicación
const app = express();

// Cargar todos los controladores disponibles para esta versión
app.use('/feeds', require('./feed'));

module.exports = app;