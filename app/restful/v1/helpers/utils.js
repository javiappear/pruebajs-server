const request = require('request');

/**
 * Transforms the request into promise
 * @param Object data={} Data to request
 * @return Promise Returns the request transformed into promise
 */
function getRequestToPromise(data = {}) {
    return new Promise((resolve, reject) => {
        request(data, function (err, response, body) {
            if (err) reject(err)
            else resolve(body);
        });
    });
}

module.exports = {getRequestToPromise};