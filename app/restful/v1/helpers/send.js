module.exports = (req, res, data = {}, status = 200, next) => {
    // Retrieve current time and minus difference
    let process_finish = new Date().getTime();

    // Return response with pre-calculated time
    res.status(status).json(Object.assign(data, {
        processTime: (process_finish - req.processInit) + 'ms'
    }));
}