// Import helper to send the response
const send = includev1('helpers/send');

/**
 * Handles controller execution and responds to user (API Express version).
 * @param promise Controller Promise.
 * @param params A function (req, res, next), all of which are optional
 */
const controllerHandler = (promise) => async (req, res, next) => {
    try {
        const result = await promise(req, res, next);

        return send(req, res, {data: result}, 200, next);
    } catch (error) {
        return send(req, res, {error: error.message}, error.status, next);
    }
};

module.exports = controllerHandler;