// Import ObjectID
const ObjectId = require('mongoose').Types.ObjectId;

// Import Feed model
const Feed = includev1('models/feed');

// Import moment to manage dates
const moment = require('moment');

/**
 * Get all the feeds from the database
 * @param Object queryParams={} Query
 * @return Promise Returns the execution of the query
 */
async function getFeeds(queryParams = {}) {
    return Feed.find(queryParams).exec();
}

/**
 * Get a feed given an ID
 * @param String id='' Feed's identifier
 * @return Promise Returns the feed fetched
 */
function getFeedById(id = '') {
    return new Promise(async (resolve, reject) => {
        if (!ObjectId.isValid(id)) return reject({status: 400, message: `El id '${id}' no es válido`});

        const feed = await Feed.findById(ObjectId(id)).exec();

        if (!feed) reject({status: 404, message: `No existe un feed con identificador ${id}`});
        else resolve(feed);
    });
}

/**
 * Insert a feed in the database
 * @param Object data={} Feed data
 * @return Promise Returns the feed inserted
 */
function insertFeed(data = {}) {
    return new Promise((resolve, reject) => {
        Feed.create(data, (error, feed) => {
            if (error) reject({status: 422, message: `No se ha podido crear el feed: ${error.message}`});
            else resolve(feed);
        });
    });
}

/**
 * Delete a feed from the database
 * @param String id='' Feed's identifier
 * @return Promise Returns if deleted
 */
function deleteFeed(id = '') {
    return new Promise(async (resolve, reject) => {
        if (!ObjectId.isValid(id)) return reject({status: 400, message: `El id '${id}' no es válido`});

        Feed.findOneAndDelete({_id: ObjectId(id)}, (error, feed) => {
            if (error) reject({status: 422, message: `No se ha podido eliminar el feed: ${error.message}`});
            else {
                if (feed) resolve(feed);
                else reject({status: 404, message: `No existe un feed con identificador ${id}`});
            }
        });
    });
}

/**
 * Update a feed from the database
 * @param String id='' Feed's identifier
 * @param Object set={} Object to update
 * @return Promise Returns object
 */
function updateFeed(id = '', set = {}) {
    return new Promise(async (resolve, reject) => {
        if (!ObjectId.isValid(id)) return reject({status: 400, message: `El id '${id}' no es válido`});

        Feed.findOneAndUpdate({_id: ObjectId(id)}, {$set: set}, {new: true}, (error, feed) => {
            if (error) reject({status: 422, message: `No se ha podido actualizar el feed: ${error.message}`});
            else {
                if (feed) resolve(feed);
                else reject({status: 404, message: `No existe un feed con identificador ${id}`});
            }
        });
    });
}

/**
 * Get today feeds from the database
 * @return Promise Returns if deleted
 */
function todayFeeds() {
    return new Promise(async (resolve, reject) => {
        const today = moment().startOf('day');

        const tomorrow = moment(today).endOf('day');

        // Set the query to get today's feeds
        const query = {
            createdAt: {
                $gte: today.toDate(),
                $lt: tomorrow.toDate()
            }
        };

        try {
            const feeds = await Feed.find(query).exec();

            resolve(feeds);

            /*const feedsWithPeriodico = feeds.map(feed => {
                // Create a copy of the object to include properties
                const feedModified = JSON.parse(JSON.stringify(feed));

                // Asign virtual property to object
                feedModified['periodico'] = (feed.toObject({virtuals: true}) || {}).periodico;

                // Return feed modified
                return feedModified;
            });

            resolve(feedsWithPeriodico);*/
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Get count of the feeds
 * @return Promise Returns count
 */
function countFeeds(queryParams = {}) {
    return new Promise((resolve, reject) => {
        Feed.countDocuments(queryParams, function (err, count) {
            if (err) reject(err);
            else
                resolve(count);
        });
    });
}

module.exports = {getFeeds, getFeedById, insertFeed, deleteFeed, updateFeed, todayFeeds, countFeeds};