// Import it to decode request's body
const iconv = require('iconv-lite');

// Server jQuery library
const cheerio = require('cheerio');

// Url's of the sources to scrap
const urls = {'el_mundo': 'http://www.elmundo.es', 'el_pais': 'https://www.elpais.com'};

// Import request to promise
const getRequestToPromise = includev1('helpers/utils').getRequestToPromise;

// Import feed service
const feedService = includev1('services/feed');

/**
 * Read feeds web scraping paper websites
 * @param String publisher='' Feed's identifier
 */
async function readAndUpdateFeeds(source = '') {
    if (!(source in urls)) return Promise.reject('Source no disponible');

    const articlesNumber = 1;

    const articlesSource = await getArticlesFromSource({source: source, articlesNumber: articlesNumber});

    for (var i = 0; i < articlesSource.length; i++) {
        const article = await getArticleFromURL({source: source, url: articlesSource[i]});

        // Insert the article in the database
        await feedService.insertFeed(article);
    }
}

/**
 * Updates the feeds of all the available sources
 */
async function updateAllSources() {
    console.log('Updating feeds from all sources...');

    try {
        for (var key in urls) {
            await readAndUpdateFeeds(key);
        }
    } catch (error) {
        console.log(`Couldn't update source ${source}, error: ${error}`);
    }
}

/**
 * Get n articles given a source
 * @param Object data={} Data to pass
 */
function getArticlesFromSource(data = {}) {
    if (!data.source) return;

    const articlesNumber = data.articlesNumber || 1;
    const articlesUrls = [];

    return new Promise(async (resolve, reject) => {
        try {
            switch (data.source) {
                case 'el_mundo': {
                    const body = await getRequestToPromise({
                        encoding: null,
                        uri: urls[data.source],
                    });

                    //Recuperar el contenido de la página codificada
                    const utf8string = iconv.decode(new Buffer(body), "ISO-8859-1");

                    const $ = cheerio.load(utf8string, {decodeEntities: false});

                    $('.content-item').each(function (index, element) {
                        // Get URL
                        const articleURL = $(element).find('header h2 a').attr('href');

                        if (articleURL) articlesUrls.push(articleURL);
                    });
                    break;
                }
                case 'el_pais': {
                    const body = await getRequestToPromise({
                        encoding: null,
                        uri: urls[data.source],
                    });

                    //Recuperar el contenido de la página codificada
                    const utf8stringPais = iconv.decode(new Buffer(body), "UTF-8");

                    const $ = cheerio.load(utf8stringPais, {decodeEntities: false});

                    $('article').each(function (index, element) {
                        // Get URL
                        const articleURL = $(element).find('.articulo-titulo a').attr('href');

                        // Append base path to url, as it's relative
                        if (articleURL) articlesUrls.push(urls[data.source] + articleURL);
                    });
                    break;
                }
                default:
                    reject('Source no disponible');
                    break;
            }

            resolve(articlesUrls.slice(0, articlesNumber));
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Creates an article given an URL
 * @param Object data={} Data to pass
 */
function getArticleFromURL(data) {
    if (!data.url || !data.source) return;

    return new Promise(async (resolve, reject) => {
        try {
            switch (data.source) {
                case 'el_mundo': {
                    const body = await getRequestToPromise({
                        encoding: null,
                        uri: data.url,
                    });

                    // Retrieve decoded content
                    const utf8string = iconv.decode(new Buffer(body), "ISO-8859-1");

                    const $ = cheerio.load(utf8string, {decodeEntities: false});

                    const article = $('article');

                    // Extract article title
                    const articleTitle = $(article).find('.js-headline').text();

                    // Extract article image
                    const articleImage = $(article).find('.full-image').attr('src');

                    // Extract article publisher
                    const articlePublisher = $(article).find('li [itemprop=name]').text();

                    // Extract article date
                    const articleDate = $('[itemprop=datePublished]').attr('content');

                    // Extract article body
                    let articleBody = '';

                    $('article p').each(function (index, element) {
                        if (!$(element).parent().hasClass('texto-comentario') && !$(element).hasClass('summary-lead')) {
                            if (index > 0) articleBody += '\n';
                            articleBody += $(element).text();
                        }
                    });

                    resolve({
                        title: articleTitle,
                        body: articleBody,
                        image: articleImage,
                        source: data.source,
                        publisher: articlePublisher
                    });
                    break;
                }
                case 'el_pais': {
                    const body = await getRequestToPromise({
                        encoding: null,
                        uri: data.url,
                    });

                    // Retrieve decoded content
                    const utf8string = iconv.decode(new Buffer(body), "UTF-8");

                    const $ = cheerio.load(utf8string, {decodeEntities: false});

                    const article = $('article');

                    // Extract article title
                    const articleTitle = $(article).find('#articulo-titulo').text();

                    // Extract article image
                    const articleImage = $(article).find('#articulo_contenedor .foto.centro img').attr('src');

                    // Extract article publishers
                    let articlePublisher = '';

                    $(article).find('.autor-nombre a').each((index, element) => {
                        if (index > 0) articlePublisher += ', ';
                        articlePublisher += $(element).text();
                    });

                    // Extract article date
                    const articleDate = $('[itemprop=datePublished]').attr('content');

                    // Extract article body
                    let articleBody = '';

                    $('article .articulo__contenedor p').each(function (index, element) {
                        if (index > 0) articleBody += '\n';
                        articleBody += $(element).text();
                    });

                    resolve({
                        title: articleTitle,
                        body: articleBody,
                        image: articleImage ? ('https:' + articleImage) : '',
                        source: data.source,
                        publisher: articlePublisher
                    });
                    break;
                }
                default:
                    console.log(`${source} is still not supported`);
                    break;
            }
        } catch (error) {
            reject(error);
        }
    });
}

module.exports = {readAndUpdateFeeds, updateAllSources};